require 'rom-sql'
require 'rom-repository'

rom = ROM.container(:sql, 'sqlite://database.sqlite') do |config|
  config.default.create_table(:learned_responses) do
    primary_key :id
    column :match, String, null: false
    column :response, String, null: false
  end

  class LearnedResponses < ROM::Relation[:sql]
    schema(infer: true)
    auto_struct true
  end

  config.register_relation(LearnedResponses)
end

class LearnedResponseRepo < ROM::Repository[:learned_responses]
  commands :create
  def query(conditions)
    learned_responses.where(conditions).to_a
  end
end

learned_responses_repo = LearnedResponseRepo.new(rom)
learned_responses_repo.create(match: "cool", response: "beans")
puts learned_responses_repo.query(match: "cool").inspect
