#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

source .env
bundle exec ruby main.rb
