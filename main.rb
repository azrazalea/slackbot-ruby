require 'slack-ruby-bot'
require 'require_all'

module SlackWorkBot
  class Bot < SlackRubyBot::Bot
  end
end

require_relative 'db/rom'
require_all 'commands'

SlackWorkBot::Bot.run
