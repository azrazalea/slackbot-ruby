module SlackWorkBot
  module Commands
    # Ping command, responds simply with pong
    class Ping < SlackRubyBot::Commands::Base
      command 'ping' do |client, data, _|
        client.say(text: 'pong',
                   channel: data.channel,
                   thread_ts: data.thread_ts)
      end
    end
  end
end
